package Okno_pozczatek;

import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class Rejestracja {
	private Stage stage;
	private Scene scene1;// scena do powrotu

	private Button createButton;
	private Button backButton;

	private TextField nickInput;
	private TextField nameInput;
	private TextField surnameInput;
	private TextField mailInput;
	private PasswordField passInput;
	private PasswordField pass2Input;
	private ChoiceBox<String> maleBox;
	private TextField yearInput;

	private Label nickLabel;
	private Label nameLabel;
	private Label surnameLabel;
	private Label mailLabel;
	private Label passLabel;
	private Label pass2Label;
	private Label maleLabel;
	private Label yearLabel;
//============================================================
	
	public Rejestracja(Stage stage,Scene scene1){
		this.stage=stage;
		this.scene1=scene1;
		
		initButtons();
		initLabels();
		initTextField();
		initLayouts();
		
		this.stage.setTitle("Rejestracja");
		this.stage.show();
		
	}
//============================================================
	private void initButtons() {
		// Create Button
		createButton = new Button("Stw�rz");
		GridPane.setConstraints(createButton, 1, 8);
		createButton.setOnAction(e -> {
			boolean result = true;
			result = isInt(yearInput, yearInput.getText());// metoda do
															// sprawdzenia czy
															// podano liczbe

			if (!result) {
				Alert.display("Error", "Invalid number");
			}

			if (validateDate(nameInput, surnameInput, passInput, mailInput) == true) {
				// laduje do bazy danych konto
				Alert.display("Konto", "Konto zosta�o pomy�lnie utworzone");
				stage.setScene(scene1);
			} else {
				Alert.display("Error", "Uzupe�nij poprawnie pola");
			}

			System.out.println("Utworzono");
		});// dopisac reszte
// ============================================================		
		// Back Button
		backButton = new Button("Powr�t");
		GridPane.setConstraints(backButton, 1, 9);
		backButton.setOnAction(e -> stage.setScene(scene1));// dopisac reszte
	

	}
//============================================================
	private void initTextField() {

// ============================================================
		// Nick_Name input
		nickInput = new TextField();
		nickInput.setPromptText("Nazwa U�ytkownika"); // sprawdzic czy nie
		GridPane.setConstraints(nickInput, 1, 0);	// istnieje juz taki
														// nickname
// ============================================================
		// First_Name input
		nameInput = new TextField();
		nameInput.setPromptText("Imie");
		GridPane.setConstraints(nameInput, 1, 1);
// ============================================================
		// Last_Name input
		surnameInput = new TextField();
		surnameInput.setPromptText("Nazwisko");
		GridPane.setConstraints(surnameInput, 1, 2);
// ============================================================
		// Email input
		mailInput = new TextField();
		mailInput.setPromptText("Email@");
		GridPane.setConstraints(mailInput, 1, 3);
// ============================================================
		// Password input
		passInput = new PasswordField();
		passInput.setPromptText("Has�o");
		GridPane.setConstraints(passInput, 1, 4);
// ============================================================
		// Confirm Password input
		pass2Input = new PasswordField();
		pass2Input.setPromptText("Powt�rz Has�o ");
		GridPane.setConstraints(pass2Input, 1, 5);
// ============================================================
		// Male/Female input
		maleBox = new ChoiceBox<>();
		maleBox.getItems().addAll("Kobieta", "M�czyzna");
		maleBox.setValue("Male"); // ustawiam default-owa wartosc
		GridPane.setConstraints(maleBox, 1, 6);
// ============================================================ // Rok urodzenia
		// Year input
		yearInput = new TextField();
		yearInput.setPromptText("Rok Urodzenia ");
		GridPane.setConstraints(yearInput, 1, 7);
	}
	
//============================================================
	private void initLabels() {
		
		//Nick_Name Label
		nickLabel = new Label("Nazwa U�ytkownika: ");
		GridPane.setConstraints(nickLabel,0,0);
// ============================================================
		// First_Name Label
		nameLabel = new Label("Imie: ");
		GridPane.setConstraints(nameLabel, 0, 1);
// ============================================================
		// Last_Name Label
		surnameLabel = new Label("Nazwisko: ");
		GridPane.setConstraints(surnameLabel, 0, 2);
// ============================================================
		// Email_Name Label
		mailLabel = new Label("Email: ");
		GridPane.setConstraints(mailLabel, 0, 3);
// ============================================================
		// Password Label
		passLabel = new Label("Has�o: ");
		GridPane.setConstraints(passLabel, 0, 4);
// ============================================================
		// Confirm Password Label
		pass2Label = new Label("Powt�rz Has�o : "); // napisac funkcje
		GridPane.setConstraints(pass2Label, 0, 5);			// ktora spawdza
															// zgodnosc podancyh
															// hasel!!
// ============================================================
		// Male/Female Label
		maleLabel = new Label("P�e�: ");
		GridPane.setConstraints(maleLabel, 0, 6);

// ============================================================ Male i Female
		// Year Label
		yearLabel = new Label("Rok Urodzenia: ");
		GridPane.setConstraints(yearLabel, 0, 7);
// ============================================================
	}
//============================================================
	private void initLayouts() {
		GridPane grid = new GridPane();
		grid.setPadding(new Insets(10, 10, 10, 10));
		grid.setVgap(10);
		grid.setHgap(10);
		
		grid.getChildren().addAll(nickLabel,nickInput,nameLabel,nameInput,surnameLabel,surnameInput,mailLabel,mailInput,passLabel,passInput,pass2Label,pass2Input,maleLabel,maleBox,yearLabel,yearInput,createButton,backButton);
		Scene scene;
		scene = new Scene(grid,400,400);
		stage.setScene(scene);
		
	}

// ============================================================		
	//Sprawdza czy podano liczbe
	private static boolean isInt(TextField input,String message){
		try {
			int age = Integer.parseInt(input.getText());
			System.out.println("Wiek uzytkownika:"+ age );
			return true;
		} catch (NumberFormatException e) {
			System.out.println();
			return false;
		}
	}
// ============================================================	
	//Validacja zmiennych 
	private static boolean validateDate(TextField nameInput,TextField surnameInput,TextField passInput,TextField mailInput){
		boolean error = false;
		
		if(Validation.nameValidation(nameInput.getText()) && Validation.surnameValidation(surnameInput.getText()) && Validation.passwordValidation(passInput.getText()) && Validation.emailValidation(mailInput.getText())){
			error = true;
		}
		
		colorTextField(nameInput, surnameInput, passInput, mailInput);
		
		return error;
	}
// ============================================================	
	//Set Color Background
	private static void colorTextField(TextField nameInput,TextField surnameInput,TextField passInput,TextField mailInput){
		if(Validation.nameValidation(nameInput.getText()) == false){
			nameInput.setStyle("-fx-background-color: #ed7765;");
		}else{
			nameInput.setStyle("-fx-background-color: #83ed65;");
		}
		
		if(Validation.surnameValidation(surnameInput.getText()) == false){
			surnameInput.setStyle("-fx-background-color: #ed7765;");
		}else{
			surnameInput.setStyle("-fx-background-color: #83ed65;");
		}
	
		if(Validation.passwordValidation(passInput.getText()) == false){
			passInput.setStyle("-fx-background-color: #ed7765;");
		}else{
			passInput.setStyle("-fx-background-color: #83ed65;");
		}
	
		if(Validation.emailValidation(mailInput.getText()) == false){
			mailInput.setStyle("-fx-background-color: #ed7765;");
		}else{
			mailInput.setStyle("-fx-background-color: #83ed65;");
		}

	}

}
