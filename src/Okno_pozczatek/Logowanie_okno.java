package Okno_pozczatek;



import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class Logowanie_okno extends Application {
	Stage stage;
	Scene scene1, scene2;
	private Rejestracja register;
	private GlowneMenu glowneMenu;
	
	private Label nameLabel;
	private Label passLabel;

	private TextField nameInput;
	private PasswordField passInput;

	private Button loginButton;
	private Button signButton;

	public static void main(String[] args) {
		launch(args);

	}

	@Override
	public void start(Stage primaryStage) throws Exception {

		stage = primaryStage;
		stage.setTitle("MyTravel Application");
		stage.setMinWidth(400);//zatrzymuje przed zmniejszaniem do tej wartosci podanej
		stage.setMinHeight(400);
	//============================================================
		//Do zamykania aplikacji
		stage.setOnCloseRequest(e -> {
			e.consume();
			closeProgram(); //wywolywanie metody
		});
		
		initLabel();
		initTextField();
		initButtons();
		initLayouts();

}
//============================================================		
	private void initLayouts() {
		GridPane grid = new GridPane();
		grid.setPadding(new Insets(10, 10, 10, 10));
		grid.setVgap(8);
		grid.setHgap(10);
	// ============================================================
		// Dodawanie wszystkich komponentow
		grid.getChildren().addAll(nameLabel, nameInput, passLabel, passInput, loginButton, signButton);
		scene1 = new Scene(grid, 400, 400);
		stage.setScene(scene1);
		stage.setTitle("Login");
		stage.show();

	}
//============================================================	
	private void initLabel() {
		// Name Label
		nameLabel = new Label("Username: ");
		GridPane.setConstraints(nameLabel, 0, 0);
	//============================================================
		// Password label
		passLabel = new Label("Password: ");
		GridPane.setConstraints(passLabel, 0, 1);
	}
//============================================================	
	private void initTextField() {
		// Name input
		nameInput = new TextField();
		nameInput.setPromptText("Nickname");
		GridPane.setConstraints(nameInput, 1, 0);
	//============================================================
		// Password input
		passInput = new PasswordField();
		passInput.setPromptText("Password");
		GridPane.setConstraints(passInput, 1, 1);

	}
//============================================================	
	private void initButtons() {
		// Button Login
		loginButton = new Button("Log In");
		GridPane.setConstraints(loginButton, 1, 2);
		loginButton.setOnAction(e -> {
			glowneMenu = new GlowneMenu(stage, scene1);
		});
	//============================================================
		// Button Sing up
		signButton = new Button("Sign Up");
		GridPane.setConstraints(signButton, 1, 3);
		signButton.setOnAction(e -> {
			boolean result = Asking.display("New Account", "Create New Account?");
			if (result == true) {
				register = new Rejestracja(stage, scene1);
			}
		});
	}

//============================================================
	//Zabezpieczenie do zamykania okien programy|| zadaje zapytanie czy wyjsc 
	private void closeProgram() {
		Boolean answer = Asking.display("Exit", "Do you really want to exit?");
		if(answer==true){
			stage.close();
		}
		
	}

}
