package Okno_pozczatek;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class Dodaj_przejazd {
	
	private  Stage stage;
	private  Scene sceneBack;// scena do powrotu
	private  Scene sceneLogout;

	private TextField fromInput;
	private TextField toInput;

	private Label fromLabel;
	private Label toLabel;

	private Button createTravelButton;
	

	private Button searchButton;
	private Button reservButton;
	private Button travelButton;
	private Button accountButton;
	private Button logOutButton;
	private Button backButton;

	private Label menuLabel;
//============================================================
	public Dodaj_przejazd(Stage stage, Scene sceneWyloguj,Scene sceneBack) {
		this.stage = stage;
		this.sceneLogout = sceneWyloguj;
		this.sceneBack = sceneBack;

		initLayouts();

	}
//============================================================
	private void initTextField() {
		// From input
		fromInput = new TextField();
		fromInput.setPromptText("Nazwa Miasta");
		GridPane.setConstraints(fromInput, 0, 1);
	//============================================================
		// To input
		toInput = new TextField();
		toInput.setPromptText("Nazwa Miasta");
		GridPane.setConstraints(toInput, 1, 1);
	}

//============================================================
	private void initLabel() {
		// From Label
		fromLabel = new Label("Z: ");
		GridPane.setConstraints(fromLabel, 0, 0);
	// ============================================================
		// To Label
		toLabel = new Label("Do: ");
		GridPane.setConstraints(toLabel, 1, 0);	
}

//============================================================
	private void initButtons() {
		// Create travel Button
		createTravelButton = new Button("Stw�rz przejazd");
		createTravelButton.setPrefSize(150, 50);
		GridPane.setConstraints(createTravelButton, 0, 2);
		createTravelButton.setOnAction(e -> {
			System.out.println("Stworzono przejazd");
		});

	}
//============================================================
	private void initLayouts() {
		GridPane gridMenu = new GridPane();
		GridPane gridTravel = new GridPane();
		gridMenu = createGridMenu();
		gridTravel = createGridTravel();
		
		BorderPane borderPane = new BorderPane();
		borderPane.setLeft(gridMenu);
		borderPane.setCenter(gridTravel);
		
		Scene scene = new Scene(borderPane,600,400);
		stage.setScene(scene);
		stage.setTitle("Dodaj Przejazd");
		stage.show();

	}
//============================================================
	public GridPane createGridTravel() {
		GridPane grid = new GridPane();
		grid.setPadding(new Insets(10, 10, 10, 10));
		grid.setVgap(8);
		grid.setHgap(10);
		grid.setAlignment(Pos.CENTER);
	// ============================================================
		initButtons();
		initLabel();
		initTextField();
	// ============================================================
		grid.getChildren().addAll(fromLabel,fromInput,toLabel,toInput,createTravelButton);
		return grid;
	}
	
// ============================================================
	private void initButtons_2() {
		
		// Search travel Button
		 searchButton = new Button("Wyszukaj Przejazd");
		searchButton.setPrefSize(150, 50);
		GridPane.setConstraints(searchButton, 0, 2);
		searchButton.setOnAction(e -> System.out.print("Szukam"));
	// ============================================================
		// Reservation Button
		reservButton = new Button("Moje Rezerwacje");
		reservButton.setPrefSize(150, 50);
		GridPane.setConstraints(reservButton, 0, 3);
		reservButton.setOnAction(e -> System.out.print("Rezerwuje"));
	// ============================================================
		// Travels Button
		travelButton = new Button("Moje Przejazdy");
		travelButton.setPrefSize(150, 50);
		GridPane.setConstraints(travelButton, 0, 4);
		travelButton.setOnAction(e -> System.out.print("Historia przejazdow"));
	// ============================================================
		// Account Button
		accountButton = new Button("Moje Konto");
		accountButton.setPrefSize(150, 50);
		GridPane.setConstraints(accountButton, 0, 5);
		accountButton.setOnAction(e -> System.out.print("Jestem kims"));
	// ============================================================
		// Log Out Button
		logOutButton = new Button("Wyloguj");
		logOutButton.setPrefSize(150, 50);
		GridPane.setConstraints(logOutButton, 0, 6);
		logOutButton.setOnAction(e -> {
			boolean result = Asking.display("Wylogowywanie", "Czy na pewno chcesz sie wylogowac?");
			if (result == true) {
				stage.setScene(sceneLogout);
			}
		});
	// ============================================================
		// Back Button
		backButton = new Button("Cofnij");
		backButton.setPrefSize(150, 50);
		GridPane.setConstraints(backButton, 0, 6);
		backButton.setOnAction(e -> {
			boolean result = Asking.display("Powr�t", "Czy na pewno chcesz sie cofn��?");
			if (result == true) {
				stage.setScene(sceneBack);
			}
		});
	}
// ============================================================
	private void initLabel_2() {
		
		// Menu Label
		menuLabel = new Label("Menu");
		menuLabel.setPrefSize(150, 50);
		GridPane.setConstraints(menuLabel, 0, 1);
	}
// ============================================================
	public GridPane createGridMenu() {
		GridPane grid = new GridPane();
		grid.setPadding(new Insets(10, 10, 10, 10));
		grid.setVgap(10);
		grid.setHgap(10);
		grid.setAlignment(Pos.CENTER);
	// ============================================================
		initButtons_2();
		initLabel_2();
	// ============================================================	
		grid.getChildren().addAll(menuLabel,searchButton,reservButton,travelButton,accountButton,logOutButton,backButton);
		return grid;
	}

}
