package Okno_pozczatek;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class GlowneMenu {
	private  Stage stage;
	private  Scene scenePowrot;
	private  Scene scene;
	
	private Button addButton;
	private Button searchButton;
	private Button reservButton;
	private Button accountButton;
	private Button travelButton;
	private Button logOutButton;
	
	private Dodaj_przejazd travel;
	
//============================================================
	public GlowneMenu(Stage stage,Scene scenePowrot){
		this.stage=stage;
		this.scenePowrot=scenePowrot;
		 initButtons();
		 initlayouts();
	}
//============================================================
	private void initlayouts() {
		GridPane grid = new GridPane();
		grid.setPadding(new Insets(10, 10, 10, 10));
		grid.setVgap(10);
		grid.setHgap(10);
		grid.setAlignment(Pos.CENTER);

		grid.getChildren().addAll(addButton, searchButton, reservButton, travelButton, accountButton, logOutButton);

		scene = new Scene(grid, 400, 400);
		stage.setScene(scene);
		stage.setTitle("Menu");
		stage.show();
	}
	
//============================================================
	private void initButtons() {
	//============================================================
		// Add travel Button
		addButton = new Button("Dodaj Przejazd");
		addButton.setPrefSize(150, 50);
		GridPane.setConstraints(addButton, 0, 1);
		addButton.setOnAction(e -> {
			travel = new Dodaj_przejazd(stage, scenePowrot, scene);
			
		}); // uzupelnic classa do dodawanaia przejazdu
	// ============================================================
		// Search travel Button
		searchButton = new Button("Wyszukaj Przejazd");
		searchButton.setPrefSize(150, 50);
		GridPane.setConstraints(searchButton, 0, 2);
		searchButton.setOnAction(e -> System.out.print("Szukam"));
	// ============================================================
		// Reservation Button
		reservButton = new Button("Moje Rezerwacje");
		reservButton.setPrefSize(150, 50);
		GridPane.setConstraints(reservButton, 0, 3);
		reservButton.setOnAction(e -> System.out.print("Rezerwuje"));
	// ============================================================
		// Travels Button
		travelButton = new Button("Moje Przejazdy");
		travelButton.setPrefSize(150, 50);
		GridPane.setConstraints(travelButton, 0, 4);
		travelButton.setOnAction(e -> System.out.print("Historia przejazdow"));
	// ============================================================
		// Account Button
		accountButton = new Button("Moje Konto");
		accountButton.setPrefSize(150, 50);
		GridPane.setConstraints(accountButton, 0, 5);
		accountButton.setOnAction(e -> System.out.print("Jestem kims"));
	// ============================================================
		// Log Out Button
		logOutButton = new Button("Wyloguj");
		logOutButton.setPrefSize(150, 50);
		GridPane.setConstraints(logOutButton, 0, 6);
		logOutButton.setOnAction(e -> {
			boolean result = Asking.display("Wylogowywanie", "Czy na pewno chcesz sie wylogowac?");
			if (result == true) {
				stage.setScene(scenePowrot);
			}
		});
	}
}
