package Okno_pozczatek;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Validation {
	//Wyrazenia regularne
	private static Pattern pattern;
	private static Matcher matcher;
//============================================================	
	private static final String NAME_PATTERN = "^[A-Za-z]{3,15}$";//od Min: 3 znakow do Max: 15
	private static final String SURNAME_PATTERN = "^[A-Za-z]{3,15}$";//od Min: 15 znakow do Max: 15
	private static final String PASSWORD_PATTERN = "^[!@#$%&*+=:;,.?<>A-Za-z0-9_-]{6,15}$";//od Min: 6 znakow do Max: 15
	private static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
	
//============================================================
	//Name Validation
	public static boolean nameValidation(final String text) {
		pattern = Pattern.compile(NAME_PATTERN);
		matcher = pattern.matcher(text);
		return matcher.matches();
	}
//============================================================
	//Surname Validation
	public static boolean surnameValidation(final String text) {
		pattern = Pattern.compile(SURNAME_PATTERN);
		matcher = pattern.matcher(text);
		return matcher.matches();
	}
//============================================================
	//Password Validation
	public static boolean passwordValidation(final String text) {
		pattern = Pattern.compile(PASSWORD_PATTERN);
		matcher = pattern.matcher(text);
		return matcher.matches();
	}
//============================================================	
	//Email Validation
	public static boolean emailValidation(final String text) {
		pattern = Pattern.compile(EMAIL_PATTERN);
		matcher = pattern.matcher(text);
		return matcher.matches();
	}
	
}
